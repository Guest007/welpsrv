# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db.models import JSONField
from django_json_widget.widgets import JSONEditorWidget
from .models import Server, Entity


@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'description', 'slug', 'is_base')
    list_filter = ('is_base',)
    search_fields = ('slug',)


@admin.register(Entity)
class EntityAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
        'description',
        'slug',
        'server',
        'value',
        'is_json',
    )
    list_display_links = ('id', 'title')
    list_filter = ('server', 'is_json')
    search_fields = ('slug', 'value')

    formfield_overrides = {
        JSONField: {'widget': JSONEditorWidget},
    }

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "server":
            fltrs = request.GET.get('_changelist_filters', '').split('&')
            for fltr in fltrs:
                if 'server__id__exact' in fltr:
                    server_id = fltr.split('=')[1]
                    kwargs["initial"] = Server.objects.get(id=server_id)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)
