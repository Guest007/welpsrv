from django.db import models
from django_extensions.db.fields import AutoSlugField
from django_extensions.db.models import TitleSlugDescriptionModel, TitleDescriptionModel


class Server(TitleSlugDescriptionModel):
    is_base = models.BooleanField(blank=True, default=False)

    def __str__(self):
        return self.slug


class Entity(TitleDescriptionModel):
    slug = AutoSlugField('slug', populate_from='title', overwrite=True, allow_duplicates=True, editable=True, separator='_')
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    value = models.TextField(default='', blank=True)
    is_json = models.BooleanField(blank=True, default=False)
    data = models.JSONField(default=dict, blank=True)

    class Meta:
        unique_together = ['slug', 'server_id']

    def __str__(self):
        return f'{self.slug} | {self.server}'
