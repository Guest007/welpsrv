from rest_framework import serializers


class SingleEntitySerializer(serializers.Serializer):
    name = serializers.SerializerMethodField()
    is_default = serializers.SerializerMethodField()
    is_json = serializers.SerializerMethodField()
    value = serializers.SerializerMethodField()

    class Meta:
        fields = ('name', 'is_default', 'is_json', 'value')

    def get_name(self, obj):
        return obj.title

    def get_is_default(self, obj):
        return obj.server.is_base

    def get_is_json(self, obj):
        return obj.is_json

    def get_value(self, obj):
        return obj.data if obj.is_json else obj.value
