from django.apps import AppConfig


class SrvdataConfig(AppConfig):
    name = 'srvdata'
