from itertools import chain
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from srvdata.models import Entity
from srvdata.serializers import SingleEntitySerializer


class SingleEntityView(APIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        server = self.kwargs.get('server')
        qs = Entity.objects.all()
        default_qs = qs.filter(server__is_base=True)
        if server and server != 'default':
            qs = qs.filter(server__slug=server)
            slugs = qs.values_list('slug', flat=True)
            default_qs = default_qs.exclude(slug__in=slugs)
            qs = list(chain(default_qs, qs))
        else:
            qs = default_qs

        return qs

    def get(self, request, **kwargs):
        self.kwargs = kwargs
        qs = self.get_queryset()
        response_data = {}
        for item in qs:
            response_data[item.slug] = SingleEntitySerializer(item).data
        return Response(response_data)
